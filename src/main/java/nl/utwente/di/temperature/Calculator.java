package nl.utwente.di.temperature;

import java.util.HashMap;

public class Calculator {

    public Calculator() {

    }
    public double calculate(String celsius) {
        double degrees = Double.parseDouble(celsius);
        return degrees * 9/5 + 32;
    }
}
